/*
 * TrackIPFSApp.h
 * Copyright, 2020-2021 Alexander von Gluck IV, all rights reserved.
 * Released under the terms of the MIT licenst
 *
 */
#ifndef _TRACKIPFS_APP_H_
#define _TRACKIPFS_APP_H_


#include "TrackIPFSCommon.h"

#include <map>
#include <AppKit.h>
#include <SupportKit.h>


class TrackIPFSApp : public BApplication
{
	//	std::map<BString, TrackIPFSWindow*> fRunningCommands;

	public:
		TrackIPFSApp();
		virtual void MessageReceived(BMessage*);
};


#endif /* _TRACKIPFS_APP_H_ */
