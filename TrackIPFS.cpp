/*
 * TrackIPFS.cpp
 * Copyright, 2020-2021 Alexander von Gluck IV, all rights reserved.
 * Released under the terms of the MIT licenst
 */

#include "TrackIPFSCommon.h"
#include "TrackIPFSApp.h"
#include "PinataAPI.h"

#include <Alert.h>
#include <Application.h>
#include <MenuItem.h>
#include <Message.h>
#include <Path.h>
#include <Roster.h>
#include <String.h>
#include <add-ons/tracker/TrackerAddOn.h>

#include <stdio.h>
#include <vector>


#define MAX_REFS_DISPLAY	6


void 
process_refs(entry_ref dir_ref, BMessage* msg, void*)
{
	PinataAPI pinata;
	if (!pinata.Ready()) {
		printf("Pinata is not ready! Exiting");
		return;
	}

	BString buffer("Upload the following files to IPFS?\n");

	int refs;
	int total = 0;

	BEntry entry(&dir_ref);
	entry_ref file_ref;
	BPath path;
	for (refs = 0; msg->FindRef("refs", refs, &file_ref) == B_NO_ERROR; refs++) {
		entry.SetTo(&file_ref);
		entry.GetPath(&path);
		if (refs < MAX_REFS_DISPLAY) {
			buffer.Append("  - ");
			buffer.Append(path.Leaf());
			buffer.Append("\n");
		}
		total++;
	}
	if (refs >= MAX_REFS_DISPLAY) {
		char more[64] = { };
		snprintf(more, sizeof(more), "  ...(and %d more)", total - MAX_REFS_DISPLAY);
		buffer.Append(more);
	}

	BAlert *alert = new BAlert("", buffer.String(), "Cancel", 
			"Upload", 0, B_WIDTH_AS_USUAL, B_WARNING_ALERT);

	if (alert->Go() == 0)
		return;

	char* CID = NULL;
	pinata.PinFile(&path, CID);
}


extern "C" void
populate_menu(BMessage* msg, BMenu* menu, BHandler* handler)
{
	// No special menus
	return;
}


extern "C" void
message_received(BMessage* msg)
{
	BMessenger messenger(APP_SIGN);

	if (!messenger.IsValid()) {
		be_roster->Launch(APP_SIGN);
		messenger = BMessenger(APP_SIGN);
	}
	messenger.SendMessage(msg);
}


int
main()
{
	TrackIPFSApp app;
	app.Run();
	return 0;
}
