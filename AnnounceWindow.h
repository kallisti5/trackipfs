/*
 * AnnounceWindow.h
 * Copyright, 2020-2021 Alexander von Gluck IV, all rights reserved.
 * Released under the terms of the MIT licenst
 */
#ifndef _ANNOUNCE_WINDOW_H
#define _ANNOUNCE_WINDOW_H


#include <Bitmap.h>
#include <Message.h>
#include <String.h>
#include <StringView.h>
#include <Window.h>


class AnnounceWindow : public BWindow {
public:
								AnnounceWindow(BMessage& resultMsg);
        virtual                 ~AnnounceWindow();

        virtual bool            QuitRequested();
        virtual void            MessageReceived(BMessage* message);

private:
		status_t				fPinResult;
		BBitmap*				fIcon;

		BString					fStatusMessage;

		BStringView*			fStatusView;
};


#endif /* ANNOUNCE_WINDOW_H */
