/*
 * AnnounceWindow.cpp
 * Copyright, 2020-2021 Alexander von Gluck IV, all rights reserved.
 * Released under the terms of the MIT licenst
 */


#include <Application.h>
#include <GroupLayout.h>
#include <GroupLayoutBuilder.h>
#include <IconUtils.h>
#include <TextControl.h>
#include <Roster.h>

#include "AnnounceWindow.h"


AnnounceWindow::AnnounceWindow(BMessage& resultMsg)
	:
	BWindow(BRect(50, 50, 500, 300), "IPFS",
		B_TITLED_WINDOW,
		B_NOT_RESIZABLE | B_ASYNCHRONOUS_CONTROLS | B_NOT_ZOOMABLE
		| B_AUTO_UPDATE_SIZE_LIMITS),
	fIcon(new BBitmap(BRect(0, 0, 15, 15), B_RGB32))
{
	CenterOnScreen();
	BGroupLayout* rootLayout = new BGroupLayout(B_VERTICAL);
	SetLayout(rootLayout);

	app_info info;
	BNode node(&info.ref);
	BIconUtils::GetVectorIcon(&node, "BEOS:ICON", fIcon);

	BString resultHash;
	status_t status = resultMsg.FindString("IpfsHash", &resultHash);

	BTextControl* ipfsHash = new BTextControl("hash", "IPFS Hash", "", NULL);
	ipfsHash->SetText(resultHash);

	BTextControl* ipfsGateway = new BTextControl("url", "Legacy URL", "", NULL);
	ipfsGateway->SetText(resultHash);

	if (status == B_OK) {
		fStatusView = new BStringView("label", "Success: Upload and pin to IPFS via Pinata complete.");
	} else {
		fStatusView = new BStringView("label", "Error: IPFS upload failed!");
		ipfsHash->SetLabel("Result");
		ipfsGateway->Hide();
	}

	rootLayout->AddItem(BGroupLayoutBuilder(B_VERTICAL, B_USE_DEFAULT_SPACING)
		.SetInsets(5, 5, 5, 5)
			.AddGroup(B_HORIZONTAL, B_USE_DEFAULT_SPACING)
			.Add(fStatusView)
			.AddGlue()
		.End()
		.Add(ipfsHash)
		.Add(ipfsGateway)
	);
}


AnnounceWindow::~AnnounceWindow()
{
}


bool
AnnounceWindow::QuitRequested()
{
	be_app->PostMessage(B_QUIT_REQUESTED);
	return true;
}


void
AnnounceWindow::MessageReceived(BMessage* message)
{
	switch (message->what) {
		default:
			BWindow::MessageReceived(message);
	}
}
