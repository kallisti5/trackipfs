/*
 * PinataAPI.cpp
 * Copyright 2014, Stephan Aßmus <superstippi@gmx.de>.
 * Copyright 2016-2021, Andrew Lindesay <apl@lindesay.co.nz>.
 * All rights reserved. Distributed under the terms of the MIT License.
 * Authors:
 *   Alexander von Gluck IV <kallisti5@unixzen.com>
 */


#include <stdio.h>

#include <Application.h>
#include <Alert.h>
#include <Bitmap.h>
#include <File.h>
#include <FindDirectory.h>
#include <IconUtils.h>
#include <Json.h>
#include <JsonMessageWriter.h>
#include <Notification.h>
#include <Path.h>
#include <Roster.h>
#include <TextView.h>	// XX drop when no longer touching BAlert TextView
#include <HttpHeaders.h>
#include <HttpRequest.h>
#include <UrlContext.h>
#include <UrlProtocolListener.h>
#include <UrlProtocolRoster.h>
#include <UrlRequest.h>

#include "AnnounceWindow.h"
#include "PinataAPI.h"

#define PINATA_API   "https://api.pinata.cloud"
#define IPFS_GATEWAY "https://gateway.pinata.cloud"
#define REPLY_LIMIT  4096


// For experimental BUrlRequest
using namespace BPrivate::Network;
using namespace BPrivate;


void
notify_system(float progress, const char* message)
{
	BNotification notification(B_PROGRESS_NOTIFICATION);
	notification.SetMessageID("pinata.cloud");
	notification.SetProgress(progress);
	notification.SetGroup("IPFS");
	notification.SetContent(message);

	app_info info;
	be_app->GetAppInfo(&info);
	BBitmap icon(BRect(0, 0, 32, 32), B_RGBA32);
	BNode node(&info.ref);
	BIconUtils::GetVectorIcon(&node, "BEOS:ICON", &icon);
	notification.SetIcon(&icon);

	notification.Send();
}


class ProtocolListener : public BUrlProtocolListener {
public:
	ProtocolListener()
	{
	}

	virtual ~ProtocolListener()
	{
	}

	virtual void ConnectionOpened(BUrlRequest* caller)
	{
		notify_system(0.0, "Connecting...");
	}

	virtual void HostnameResolved(BUrlRequest* caller, const char* ip)
	{
	}

	virtual void ResponseStarted(BUrlRequest* caller)
	{
	}

	virtual void HeadersReceived(BUrlRequest* caller, const BUrlResult& result)
	{
	}

	virtual void BytesWritten(BUrlRequest* caller, size_t bytesWritten)
	{
	}

	virtual void DownloadProgress(BUrlRequest* caller, ssize_t bytesReceived,
		ssize_t bytesTotal)
	{
		float progress = 0.25;
		if (bytesTotal > 0)
			progress = bytesReceived / (float)bytesTotal;

		notify_system(progress, "Downloading...");
	}

	virtual void UploadProgress(BUrlRequest* caller, ssize_t bytesSent,
		ssize_t bytesTotal)
	{
		float progress = 0.25;
		if (bytesTotal > 0)
			progress = bytesSent / (float)bytesTotal;

		notify_system(progress, "Uploading...");
	}

	virtual void RequestCompleted(BUrlRequest* caller, bool success)
	{
		notify_system(1.0, "Upload complete!");
	}

	virtual void DebugMessage(BUrlRequest* caller,
		BUrlProtocolDebugMessage type, const char* text)
	{
		printf("debug: %s", text);
	}
};


static BHttpRequest*
make_http_request(const BUrl& url, BDataIO* output,
    BUrlProtocolListener* listener = NULL,
    BUrlContext* context = NULL)
{
	BUrlRequest* request = BUrlProtocolRoster::MakeRequest(url, output,
		listener, context);
	BHttpRequest* httpRequest = dynamic_cast<BHttpRequest*>(request);
	if (httpRequest == NULL) {
		delete request;
		return NULL;
	}
	return httpRequest;
}


PinataAPI::PinataAPI()
	:
	fReady(false),
	fBaseURL(PINATA_API)
{
	BPath path;
	status_t status = find_directory(B_USER_SETTINGS_DIRECTORY, &path);
	if (status != B_OK) {
		printf("Error: Unable to locate B_USER_SETTINGS_DIRECTORY!\n");
		return;
	}

	path.Append("Pinata");
	BFile file;
	status = file.SetTo(path.Path(), B_READ_ONLY);
	if (status != B_OK) {
		BAlert *alert = new BAlert("", "Missing Pinata JWT key in ~/config/settings/Pinata",
			"Cancel", 0, 0, B_WIDTH_AS_USUAL, B_STOP_ALERT);
		alert->Go();
		return;
	}

	char buffer[1024] = {};
	file.Read(&buffer, sizeof(buffer));
	fJWT = buffer;
	fJWT.Prepend("Bearer ");
	fReady = true;
}


PinataAPI::~PinataAPI()
{
}


status_t
PinataAPI::_ParseResponse(BMallocIO* replyData, BMessage& response)
{
	status_t result = B_ERROR;
	replyData->Seek(0, SEEK_SET);

	BJsonMessageWriter jsonMessageWriter(response);
	BJson::Parse(replyData, &jsonMessageWriter);
	result = jsonMessageWriter.ErrorStatus();
	if (result != B_OK) {
		return result;
	}

	return B_OK;
}


void
PinataAPI::AnnounceCID(BMessage& resultMsg)
{
	// TODO: Pass BMessage to AnnounceWindow
	//AnnounceWindow* resultWindow = new(std::nothrow) AnnounceWindow(resultMsg);
	//if (resultWindow)
	//	resultWindow->Show();

	// XXX: Wait for resultWindow to close??
	BString result;

	//JSON: IpfsHash, PinSize, Timestamp
	resultMsg.FindString("IpfsHash", &result);
	result.Prepend(IPFS_GATEWAY "/ipfs/");

	// For now... a *BASIC* BAlert to be replaced by AnnounceWindow
	BAlert *alert = new BAlert("", result,
		"OK", 0, 0, B_WIDTH_AS_USUAL, B_INFO_ALERT);
	alert->TextView()->MakeSelectable(true);
	alert->Go();
}


status_t
PinataAPI::PinFile(BPath* file, char* resultCID)
{
	ProtocolListener listener;
	BUrlContext context;

	notify_system(0.0, "Preparing...");

	BUrl url = GetUri("/pinning/pinFileToIPFS");

	BHttpHeaders headers;
	headers.AddHeader("Authorization", fJWT);

	BHttpRequest* request = make_http_request(url, NULL, &listener, &context);

	if (request == NULL)
		return B_ERROR;

	BHttpForm form;
	form.AddFile("file", *file);

	request->SetMethod(B_HTTP_POST);
	request->SetHeaders(headers);
	request->SetPostFields(form);

	BMallocIO replyData;
	request->SetOutput(&replyData);

	thread_id thread = request->Run();
	wait_for_thread(thread, NULL);

	const BHttpResult& result = dynamic_cast<const BHttpResult&>(
		request->Result());

	switch (result.StatusCode()) {
		case B_HTTP_STATUS_OK:
			break;

		case B_HTTP_STATUS_UNAUTHORIZED:
		{
			BAlert* alert = new BAlert("", "Pinata: Unauthorized",
				"OK", 0, 0, B_WIDTH_AS_USUAL, B_STOP_ALERT);
			alert->Go();
			return B_ERROR;
		}
		default:
		{
			BAlert* alert = new BAlert("", "Pinata: Upload failed!",
				"OK", 0, 0, B_WIDTH_AS_USUAL, B_STOP_ALERT);
			alert->Go();
			return B_ERROR;
		}
	}

	BMessage resultMsg;
	if (_ParseResponse(&replyData, resultMsg) != B_OK) {
		BAlert* alert = new BAlert("", "Pinata: Error reported!",
			"OK", 0, 0, B_WIDTH_AS_USUAL, B_STOP_ALERT);
		alert->Go();
		return B_ERROR;
	}

	AnnounceCID(resultMsg);

	return B_OK;
}


status_t
PinataAPI::PinDir(BPath* dir, char* resultCID)
{
	return B_OK;
}
