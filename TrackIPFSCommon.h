/*
 * TrackIPFS.h
 * Copyright, 2020-2021 Alexander von Gluck IV, all rights reserved.
 * Released under the terms of the MIT licenst
 *
 * Some portions from TrackGit add-on by
 *   Hrishikesh Hiraskar <hrishihiraskar@gmail.com>
 */
#ifndef _TRACKIPFS_H_
#define _TRACKIPFS_H_


#define ADDON_NAME "IPFS"
#define APP_SIGN "application/x-vnd.Haiku-TrackIPFS"


#endif /* _TRACKIPFS_H_ */
