/*
 * PinataAPI.h
 * Copyright, 2020-2021 Alexander von Gluck IV, all rights reserved.
 * Released under the terms of the MIT licenst
 */

#include <Path.h>
#include <String.h>
#include <Url.h>
#include <SupportDefs.h>
#include <Message.h>


class PinataAPI {
public:
				PinataAPI();
				~PinataAPI();

	status_t	PinFile(BPath* file, char* resultCID);
	status_t	PinDir(BPath* dir, char* resultCID);

	bool		Ready() { return fReady; }

	BUrl		GetUri(BString uri) { return BUrl(fBaseURL, uri); }

	status_t	_ParseResponse(BMallocIO* requestData, BMessage& response);
	void		AnnounceCID(BMessage& uploadResult);

private:
	bool		fReady;
	BUrl		fBaseURL;
	BString		fJWT;
};
