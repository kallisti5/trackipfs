# TrackIPFS

A [Haiku](https://haiku-os.org) Tracker add-on to upload files to IPFS through Pinata.cloud

![TrackIPFS](Docs/IPFSUpload.png)

![TrackIPFS](Docs/IPFSUploaded.png)

## Usage

* Install as a pkg
* Compile, and copy UploadIPFS to ~/config/non-packaged/add-ons/Tracker/

## Configuration

* Generate a JWT token from https://pinata.cloud
* Place JWT token into ~/config/settings/Pinata

## TODO

* Improved "Announcement window" of IPFS pin result.
* Multi-file / Directory uploads
* Improve dialogs (more than a BAlert)
* Other pinning services?

## Warnings

* Your API JWT Token is unencrypted in ~/config/settings/Pinata and gives anyone access to your account

## License

Released under the terms of the MIT license
